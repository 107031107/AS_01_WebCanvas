# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Rainbow paint                                    | 1~5%     | Y         |


---

### How to use 

    Basic control tools:
        右上角menu從上而下依序為 : 選擇顏色(調色盤)
                                選擇線條粗細
                                選擇字形
                                選擇字體大小
        右邊工具欄左上為paint 右上為eraser 點擊後即可使用    
        
    Text input
        右上角menu可選擇字體大小及字形
        右邊工具欄"Text"選項點擊即可使用
        使用:
            點擊畫板就會出現打字欄 輸入並按下enter後即會在點擊位置輸出文字
            
    Cursor icon
        使用 "Paint", "Eraser", "Text", "Circle", "Rectangle", "Triangle", "Rainbow"
        都會出現對應的cursor icon
        
    Refresh button
        底下欄位中 中間的按鈕(有"X"的按鈕) 點擊即可Refresh
        
    Different brush shapes
        右邊工具欄"Circle", "Rectangle", "Triangle" 選項點擊即可使用
        "Circle": 
            會將首次點擊的地方當作圓心, 鼠標現在所在位置為外圓作圖
            放開後將會輸上畫板
        "Rectangle" :
            會將首次點擊的地方當作一點, 鼠標現在所在位置為其對角線做矩形
            放開後將會輸上畫板
        "Triangle" : 
            會將首次點擊的地方當作一點, 與鼠標現在所在位置連成一線, 當等腰三角形的腰,並做等腰三角形
            放開後將會輸上畫板
            
    Un/Re-do button
        底下欄位中 左邊的按鈕(有"<-"的按鈕) 點擊即可Redo
                 右邊的按鈕(有"->"的按鈕) 點擊即可Undo
    Image tool
        右邊工具欄 "Image" 選項點擊即可載入圖片
        
    Download
        右邊工具欄 "Download" 選項點擊即可儲存畫板上的圖片
    
### Function description

    Rainbow:
        右邊工具欄 "Rainbow" 選項點擊即可使用彩色畫筆功能
        彩色畫筆功能:
            從紅色開始, 移動久了就會出現紅橙黃綠藍靛紫的顏色

### Gitlab page link

   https://gitlab.com/107031107/AS_01_WebCanvas

### Others (Optional)

    助教好帥好漂亮

<style>
table th{
    width: 100%;
}
</style>
