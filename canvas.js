
const cv = document.getElementById('drawing_board');
const bd = document.body;
const board = document.getElementById("board");
const ft = document.getElementsByName("font");

const size_bar = document.getElementById("size_selector");
const ft_z = document.getElementById("font_size");
const color = document.getElementById("color");


const imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', image, false);

if (cv.getContext) {
    var ctx = cv.getContext('2d');
}

ctx.beginPath();


ctx.lineWidth = 1;
ctx.font = "30pt Arial";
input_string = "";
ctx.text = false;
ctx.paint = false;
ctx.lineCap = "round";
ctx.erase = false;
ctx.circle = false;
ctx.rectangle = false;
ctx.triangle = false;
ctx.size_select = false;
ctx.rainbow = false;
cv.style.cursor = "url(paint.cur) 0 2000,auto";
ctx.strokeStyle = "#000000";

ctx.rainbow_tmp = 0;

cv_arr = new Array();
step = 0;
cv_arr.push(cv.toDataURL());


// tool select
var tool = 1; // 1:paint ; 2:eraser ; 3:text ; 4:circle ; 5:rectangle ; 6:triangle

function select_size() {
    if(ctx.size_select)
        ctx.lineWidth = size_bar.value;
}

function select_font() {
    console.log(ctx.font);
    ctx.font = ft_z.value + "pt " + ft[0].value;
    console.log(ctx.font);
}

function select_color(e) {
    console.log(color.value);
    ctx.strokeStyle = color.value;
    ctx.fillStyle = color.value;

}

function redo() {

    if (step + 1 < cv_arr.length) {
        step++;
        if (tool == 2) {
            console.log("erase mode!");
            ctx.globalCompositeOperation = "source-over";
        }
        var cv_pic = new Image();
        cv_pic.src = cv_arr[step];

        refresh();
        cv_pic.onload = function () {
            ctx.drawImage(cv_pic, 0, 0);
            if (tool == 2)
                ctx.globalCompositeOperation = "destination-out"
        };
        

    }

}

function undo() {
    console.log("undo... step : " + step);
    if (step > 0) {

        step--;

        ctx.globalCompositeOperation = "source-over";
        refresh();
        var cv_pic = new Image();
        cv_pic.src = cv_arr[step];

        cv_pic.onload = function () {
            ctx.drawImage(cv_pic, 0, 0);
            if (tool == 2)
            ctx.globalCompositeOperation = "destination-out"
        }
        
    }
}

function image(e) {
    if (tool == 2) {
        console.log("erase mode!");
        ctx.globalCompositeOperation = "source-over";
    }
    var reader = new FileReader();
    reader.onload = function (event) {
        var img = new Image();
        img.onload = function () {
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
        if (tool == 2)
            ctx.globalCompositeOperation = "destination-out"
    }
    reader.readAsDataURL(e.target.files[0]); 

    step++;
    push_undo_stack();
}

function download() {

}

function refresh() {
    ctx.clearRect(0, 0, cv.width, cv.height);
}


function paint(e) {
    if (e == undefined || !ctx.paint) return;

    var x = cv.getBoundingClientRect().x;

    console.log(cv.width, cv.height);
    var x = (e.clientX - 350);
    var y = (e.clientY - 50);

    ctx.beginPath();
    ctx.moveTo(ctx.lastx, ctx.lasty);
    ctx.lineTo(x, y);
    ctx.stroke();
    ctx.lastx = x;
    ctx.lasty = y;
    console.log("drawing...");
    console.log("x, y : " + x + " , " + y);
    console.log("mouse position:" + e.clientX + " , " + e.clientY);
}

function rainbow_paint(e) {
    if (e == undefined || !ctx.rainbow) return;

    var x = (e.clientX - 350);
    var y = (e.clientY - 50);

    ctx.lineCap = "round";

    
        console.log("rainbow:" + ctx.rainbow_tmp);
        if (ctx.rainbow_tmp < 256) {
            ctx.strokeStyle = "rgb(" + 255 + ","
                + ctx.rainbow_tmp + ","
                + 0 + ")";
            ctx.rainbow_tmp++;
        }
        else if (ctx.rainbow_tmp < 512) {
            ctx.strokeStyle = "rgb(" + (511 - ctx.rainbow_tmp) + ","
                + 255 + ","
                + 0 + ")";
            ctx.rainbow_tmp++;
        }
        else if (ctx.rainbow_tmp < 768) {
            ctx.strokeStyle = "rgb(" + 0 + ","
                + 255 + ","
                + (ctx.rainbow_tmp - 512) + ")";
            ctx.rainbow_tmp++;
        }
        else if (ctx.rainbow_tmp < 1024) {
            ctx.strokeStyle = "rgb(" + 0 + ","
                + (1023 - ctx.rainbow_tmp) + ","
                + 255 + ")";
            ctx.rainbow_tmp++;
        }
        else if (ctx.rainbow_tmp < 1280) {
            ctx.strokeStyle = "rgb(" + (ctx.rainbow_tmp - 1024) + ","
                + 0 + ","
                + 255 + ")";
            ctx.rainbow_tmp++;
        }
        else if (ctx.rainbow_tmp < 1536) {
            ctx.strokeStyle = "rgb(" + 255 + ","
                + 0 + ","
                + (1535 - ctx.rainbow_tmp) + ")";
            ctx.rainbow_tmp++;
    }
    else if (ctx.rainbow_tmp >= 1536) ctx.rainbow_tmp = 0;

    ctx.strokeStyle = ctx.rainbow_tmp;


    ctx.beginPath();
    ctx.moveTo(ctx.lastx, ctx.lasty);
    ctx.lineTo(x, y);
    ctx.stroke();
    ctx.lastx = x;
    ctx.lasty = y;
    console.log("drawing...");
    console.log("x, y : " + x + " , " + y);
    console.log("mouse position:" + e.clientX + " , " + e.clientY);

    
}


function erase(e) {
    if (e == undefined || !ctx.erase) return;

    var x = (e.clientX - 350);
    var y = (e.clientY - 50);

    ctx.lineCap = "round";
    ctx.lineTo(x, y);
    ctx.stroke();
    console.log("erasing...");
    console.log("x, y : " + x + " , " + y);
    console.log("mouse position:" + e.clientX + " , " + e.clientY);
}

function text() {
    console.log(ctx.lastx, ctx.lasty);
    ctx.fillText(input_string, ctx.lastx, ctx.lasty);
}

function circle(e) {
    if (e == undefined || !ctx.circle) return;

    var x = (e.clientX - 350);
    var y = (e.clientY - 50);

    refresh();
    var cv_pic = new Image();
    cv_pic.src = cv_arr[step];
    ctx.drawImage(cv_pic, 0, 0);

    ctx.beginPath();
    ctx.arc(ctx.lastx, ctx.lasty,
        Math.sqrt(Math.pow(ctx.lastx - x, 2) + Math.pow(ctx.lasty - y, 2)),
        0, 2 * Math.PI);
    ctx.stroke();

    console.log("circling...");
    console.log("x, y : " + x + " , " + y);
    console.log("mouse position:" + e.clientX + " , " + e.clientY);


}


//記 str前的圖片 每畫一move就蓋上去 直到fin
function rectangle(e) {
    if (e == undefined || !ctx.rectangle) return;

    var x = (e.clientX - 350);
    var y = (e.clientY - 50);

    refresh();
    var cv_pic = new Image();
    cv_pic.src = cv_arr[step];
    ctx.drawImage(cv_pic, 0, 0);

    ctx.strokeRect(ctx.lastx, ctx.lasty, x - ctx.lastx, y - ctx.lasty);
    
    console.log("rectangling...");
    console.log("x, y : " + x + " , " + y);
    console.log("mouse position:" + e.clientX + " , " + e.clientY);
}

function triangle(e) {

    if (e == undefined || !ctx.triangle) return;

    var x = (e.clientX - 350);
    var y = (e.clientY - 50);

    refresh();
    var cv_pic = new Image();
    cv_pic.src = cv_arr[step];
    ctx.drawImage(cv_pic, 0, 0);

    // Stroked triangle
    ctx.beginPath();
    ctx.moveTo(ctx.lastx, ctx.lasty);
    ctx.lineTo(x, y);
    ctx.lineTo(2 * ctx.lastx - x, y);
    ctx.closePath();
    ctx.stroke();

    console.log("triangling...");
    console.log("x, y : " + x + " , " + y);
    console.log("mouse position:" + e.clientX + " , " + e.clientY);
}


function line(e) {

    if (e == undefined || !ctx.triangle) return;

    var x = (e.clientX - 350);
    var y = (e.clientY - 50);

    refresh();
    var cv_pic = new Image();
    cv_pic.src = cv_arr[step];
    ctx.drawImage(cv_pic, 0, 0);

    // Stroked triangle
    ctx.beginPath();
    ctx.moveTo(ctx.lastx, ctx.lasty);
    ctx.lineTo(x, y);
    //ctx.lineTo(2 * ctx.lastx - x, y);
    //ctx.closePath();
    ctx.stroke();

    console.log("lining...");
    console.log("x, y : " + x + " , " + y);
    console.log("mouse position:" + e.clientX + " , " + e.clientY);
}




function show_input_box(e) {

    input_tag = document.createElement("input");
    input_tag.type = "text";
    input_tag.style.position = "fixed";
    input_tag.style.left = e.clientX+"px";
    input_tag.style.top = e.clientY+"px";

    board.appendChild(input_tag);
}

function push_undo_stack() {

    step++;
    if (step != cv_arr.length) {
        cv_arr.length = step;
    }
    cv_arr.push(cv.toDataURL());
    console.log("get data success");
}


function select_tool(num) {
    tool = num;
    ctx.beginPath();

    ctx.paint = false;
    ctx.erase = false;

    if (ctx.text === true) {
        input_tag.remove();
        ctx.text = false;
    }

    ctx.strokeStyle = color.value;

    ctx.globalCompositeOperation = "source-over";
    switch (num) {
        case 1:
            cv.style.cursor = "url(paint.cur) 0 2000,auto";
            break;
        case 2:
            cv.style.cursor = "url(eraser.cur) ,auto";;
            ctx.globalCompositeOperation = "destination-out";
            break;
        case 3:
            cv.style.cursor = "text";
            break;
        case 4:
            cv.style.cursor = "url(circle.cur),default";
            
            break;
        case 5:
            cv.style.cursor = "url(rectangle.cur),default";
            break;
        case 6:
            cv.style.cursor = "url(triangle.cur),default";
            break;
        case 7:
            cv.style.cursor = "url(rainbow_paint.cur) 0 2000,default";
    } 
    console.log("now the tool is:" + tool);
}





function str_pos(e) {
    var x = (e.clientX - 350);
    var y = (e.clientY - 50);
    ctx.lastx = x;
    ctx.lasty = y;
    switch (tool) {
        case 1:
            ctx.paint = true;
            break;
        case 2:
            ctx.erase = true;
            break;
        case 3:
            if (ctx.text == true) {
                input_tag.remove();
            }
            show_input_box(e);
            ctx.text = true;
            break;
        case 4:
            ctx.circle = true;
            break;
        case 5:
            ctx.rectangle = true;
            
            break;
        case 6:
            ctx.triangle = true;
            break;
        case 7:
            ctx.rainbow = true;
            break;
    }    
}

function fin_pos(e) {
    switch (tool) {
        case 1:
            if (ctx.paint == true) push_undo_stack();
            ctx.paint = false;
            break;
        case 2:
            if (ctx.erase == true) push_undo_stack();
            ctx.erase = false;
            break;
        case 3:
            break;
        case 4:
            if (ctx.circle == true) push_undo_stack();
            ctx.circle = false;
            break;
        case 5:
            if (ctx.rectangle == true) push_undo_stack();
            ctx.rectangle = false;
            break;
        case 6:
            if (ctx.triangle == true) push_undo_stack();
            ctx.triangle = false;
            break;
        case 7:
            if (ctx.rainbow == true) push_undo_stack();
            ctx.rainbow = false;
            break;
    } 
    
    ctx.size_select = false;
    ctx.beginPath();
}

function move(e) {
    switch (tool) {
        case 1:
            paint(e);
            break;
        case 2:
            erase(e);
            break;
        case 3:
            break;
        case 4:
            
            circle(e);
            break;
        case 5:
            rectangle(e);
            break;
        case 6:
            triangle(e);
            break;
        case 7:
            rainbow_paint(e);
            
    } 
}


function type(e) {
    console.log(e.key);
    if (e.key == "Enter") {
        if (ctx.text === true) {
            ctx.text = false;
            input_string = input_tag.value;
            input_tag.remove();
            text();
            push_undo_stack();
        }
    }

    
}



cv.addEventListener("mousedown", str_pos);
document.addEventListener("keydown", type);
bd.addEventListener("mouseup", fin_pos);
cv.addEventListener("mousemove", move);
size_bar.addEventListener("mousedown", () => {ctx.size_select = true});
size_bar.addEventListener("mousemove", select_size);
ft[0].addEventListener("change", select_font);
ft_z.addEventListener("change", select_font);

color.addEventListener("change", select_color);


